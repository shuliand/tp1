package modelo;

public class TableroDeLuces {
	
    private Luz[][]tableroDeLuces; 
    private int tamanio;
    private int cantLuces;
    private  int lucesApagadas;
    private boolean ganaste;
    
    public TableroDeLuces(int tamanio) {
    	this.lucesApagadas=0;
    	this.tamanio=tamanio;
    	this.cantLuces=tamanio*tamanio;
    	this.tableroDeLuces = new Luz[tamanio][tamanio];
    	this.ganaste=false;
    	generarMatriz(tamanio);
    }
    
    public void alternar(int fila, int columna) {
		for(int i=0; i<tableroDeLuces.length;i++) {
			for(int j=0; j<tableroDeLuces[i].length;j++) {
				if (fila==i || columna==j) {
					tableroDeLuces[i][j].interruptor();
					if (tableroDeLuces[i][j].estaPrendida())
						lucesApagadas--;
					else 
						lucesApagadas++;
				}
			}
		}			
		if (lucesApagadas==cantLuces) {
			this.ganaste=true;
		}
		
		//		PARA PROBAR GANANDO MAS FACIL		
//		if (lucesApagadas>(cantLuces)/2) {
//			this.ganaste=true;
//		}
	} 
    
    private void generarMatriz(int tamanio) {
		for (int i=0; i<tamanio;i++) {
			for (int j=0; j<tamanio;j++) {
				boolean estadoInicialDeLuz = estadoInicialAleatorio();
	            if(estadoInicialDeLuz==false) {
	                this.lucesApagadas++;
	            }
	            tableroDeLuces[i][j]=new Luz(estadoInicialDeLuz);
			}
		}
	}
    
    private boolean estadoInicialAleatorio() {
    	if(Math.random()>0.5) 
    		return true;
    	else 
    		return false;
    }

    //getters
    public Luz[][] getTablero() {
       return tableroDeLuces; 
    }
    
    public boolean getGanaste() {
    	return ganaste;
    }
    
    public int getTamanio() {
		return tamanio;
	}

    public int getLucesApagadas() {
  		return lucesApagadas;
  	}

	public boolean estaPrendida(int fila, int col) {
		return this.tableroDeLuces[fila][col].estaPrendida();
	}
	
	public String toString() {
		StringBuilder s= new StringBuilder();
		for(int i=0; i<tableroDeLuces.length;i++) {
			for(int j=0; j<tableroDeLuces[i].length;j++) {			
					if (tableroDeLuces[i][j].estaPrendida()) {
						s.append("1  ");
					}else { 
						s.append("0  ");
					}					
			}
			s.append("\n");
		}
		return s.toString();
	}
}
