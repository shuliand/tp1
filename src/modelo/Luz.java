package modelo;

public class Luz {
    private boolean estado;
    
    public Luz(boolean estadoInicial) {
        estado = estadoInicial;
    }
    
    public void interruptor() {
    	estado  = !estado;
    }
    
    public boolean estaPrendida() {
        return estado;
    }
    
    public String toString() {
    	if(estado) {
    		return "1";
    	}return "0";
    	
    }
}
