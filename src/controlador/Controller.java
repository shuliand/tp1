package controlador;
import modelo.Luz;
import modelo.TableroDeLuces;

public class Controller {
	private TableroDeLuces juego;
	

	public Controller(int size) {
		this.juego = new TableroDeLuces(size);
	}
	
	public String getMatrizString() {
		return juego.toString();
	}
	
	public Luz[][] getTablero() {
	       return this.juego.getTablero(); 
	}
	
	public void alternar(int x, int y) {
		 this.juego.alternar(x, y);
	}
	
	public int tamanio() {
		return this.juego.getTamanio();
	}
	
	public boolean getGanaste() {
	    	return juego.getGanaste();
	}
	
	public int getLucesApagadas() {
  		return this.juego.getLucesApagadas();
  	}
	
	public boolean luzEstaPrendida(int fila, int columna) {
		return this.juego.estaPrendida(fila, columna);
	}

	
}
