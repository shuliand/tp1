package controlador;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MejoresPuntosController {
    private String mejoresPuntosPath = MejoresPuntosController.class.getResource("./mejores_puntos/puntos.txt").getPath();
    private String mejoresNombresPath= MejoresPuntosController.class.getResource("./mejores_puntos/nombres.txt").getPath();

    private String nombreDeUsuario = "";
    private int puntosDeUsuario;


    private ArrayList<String> nombresTop;
    private ArrayList<Integer> puntosTop;
    private int posicionDeUsuario;
    private boolean entraEnTop;

    public MejoresPuntosController(int puntosDeUsuario) {
        this.puntosDeUsuario = puntosDeUsuario;
        nombresTop = getNombres();
        puntosTop = getPuntos();
        this.posicionDeUsuario = posicionDeTop();
        entraEnTop = posicionDeUsuario >=0;
    }

    public String getNombreDeUsuario() {
        return nombreDeUsuario;
    }
    public void guardarNuevoTop() {
       if(nombreDeUsuario.length() > 2) {
         ArrayList<Integer> nuevosPuntos = new ArrayList<>();
        ArrayList<String> nuevosNombres = new ArrayList<>();
            
        boolean usuarioAgregado = false;
            
        for (int i = 0; i < this.puntosTop.size(); i++) {
            if (!usuarioAgregado && i == posicionDeUsuario) {
                nuevosPuntos.add(puntosDeUsuario);
                nuevosNombres.add(nombreDeUsuario);
                usuarioAgregado = true;
            } else if(i != puntosTop.size()){
                nuevosPuntos.add(this.puntosTop.get(i));
                nuevosNombres.add(this.nombresTop.get(i));
            }
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(mejoresNombresPath))) {
            StringBuilder nombres = new StringBuilder();
            for (int i = 0; i < nuevosNombres.size(); i++) {
                nombres.append(nuevosNombres.get(i) + "\n");
            }
            writer.flush();
            writer.write(nombres.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(mejoresPuntosPath))) {
            StringBuilder puntos = new StringBuilder();
            for (int i = 0; i < nuevosPuntos.size(); i++) {
                puntos.append(nuevosPuntos.get(i) + "\n");
            }
            writer.flush();
            writer.write(puntos.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
       }
        
    }
    private int posicionDeTop(){
        int pos = -1;
        for (int i = 0; i < puntosTop.size(); i++) {
          if(this.puntosDeUsuario < puntosTop.get(i)){
                return i;
          }
        }
        return pos;
    }

    public void agregarLetra(String nuevaLetra) {
        if(nombreDeUsuario.length() < 5){
           nombreDeUsuario +=nuevaLetra;
        }
    }
    public void eliminarLetra() {
        if (nombreDeUsuario.length() > 0) {
            nombreDeUsuario = nombreDeUsuario.substring(0, nombreDeUsuario.length() - 1);
        }
    }
    //getters
   
    public boolean getEntraEnTop() {
        return entraEnTop;
    }
    
    public ArrayList<String> getNombresTop() {
        return nombresTop;
    }
    public ArrayList<Integer> getPuntosTop() {
        return puntosTop;
    }
    //leyendo archivos//
    private ArrayList<String> getNombres(){
        ArrayList<String>nombres = new ArrayList<String>();
            try {
            // Abre el archivo
            File file = new File(mejoresNombresPath);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                nombres.add(line);
            }

            // Cierra el BufferedReader
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nombres;
    }
    private ArrayList<Integer> getPuntos(){
        ArrayList<Integer>puntos = new ArrayList<Integer>();
            try {
            // Abre el archivo
            File file = new File(mejoresPuntosPath);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                puntos.add(Integer.parseInt(line));
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return puntos;
    }
    //leyendo archivos//
}
