package vista;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;

public class Fuente {
    static String fuentePath  = "./local/fuente/pixel_font.ttf";


   public static Font cargarFuente() {
        try {
			File fontFile = new File(Fuente.class.getResource(fuentePath).getPath());

            Font nuevaFuente = Font.createFont(Font.TRUETYPE_FONT, fontFile);
            return nuevaFuente.deriveFont(Font.PLAIN, 14); 
        } catch (IOException | FontFormatException e) {
            e.printStackTrace();
            //devolvemos esta en caso de que no exista la custom!
            return new Font("SansSerif", Font.PLAIN, 14);
        }
    }
}
