package vista;
 
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;



@SuppressWarnings("serial")
public class View extends JFrame{
	private int altura = 800;
	private int ancho = 1200;
	private Font fuente;
	private PanelGrafico panelGrafico;
	JLabel elegirDificultalLabel;
	JButton facilBtn;
	JButton normalBtn;
	JButton dificilBtn;

	 
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		View view = new View();
	}
 
	public View() {
		this.fuente = loadCustomFont();
		setUIFont(fuente);
		initialize();
	}

	private void initialize() {
		setResizable(false);
		pack();
		setBounds(0, 0, ancho, altura);
		setLayout(null);
		setLocationRelativeTo(null); // centro la ventana
		 
		try {
			final Image backgroundImage = javax.imageio.ImageIO.read(getClass().getResource("./local/imagenes/bg.jpg"));
			setContentPane(new JPanel(new BorderLayout()) {
				@Override public void paintComponent(Graphics g) {
					g.drawImage(backgroundImage, 0, 0, null);
				}
			});
		} catch (IOException e) {
			throw new RuntimeException(e);
		}	 
	
		JLabel nombreDeJuegoLabel = new JLabel();
		setNombreDeJuego(nombreDeJuegoLabel);
		
		setVisible(true);
		setDificultad();
	}
	
	private void setDificultad() {
		this.elegirDificultalLabel = new JLabel();
		setLabelElegirDificultad(elegirDificultalLabel);
		
		this.facilBtn=new JButton();
		setBotonDificultad(facilBtn,"Fácil 4x4");
		facilBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarFrame();
				iniciarPanel(4); 
			}
		});
		this.normalBtn=new JButton();
		setBotonDificultad(normalBtn, "Normal 5x5");
		normalBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				limpiarFrame();
				iniciarPanel(5);
			}
		});	
		this.dificilBtn=new JButton();
		setBotonDificultad(dificilBtn, "Difícil 6x6");
		dificilBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarFrame();
				iniciarPanel(6);
			}
		});
	}
	
	private void setLabelElegirDificultad(JLabel label) {
		label.setText("Elegir dificultad");
		label.setFont(new Font("Tahoma", Font.BOLD, 16));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(10, 176, 764, 27);
	}
	
	private void setBotonDificultad(JButton boton, String texto) {
		boton.setText(texto);
		boton.setBounds(SwingConstants.CENTER,100,170,40);
		if (boton.equals(facilBtn)) {
			boton.setBounds(getCentro(170), 300, 170, 40);
		}else if (boton.equals(normalBtn)) {
			boton.setBounds(getCentro(170), 400, 170, 40);	
		}else {
			boton.setBounds(getCentro(170), 500, 170, 40);
		}
		add(boton);
	}
	
	private void setNombreDeJuego(JLabel label) {
		label.setFont(fuente.deriveFont(Font.PLAIN, 50));
		label.setText("LIGHTS OUT");
		label.setForeground(Color.white);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.TOP);
		add(label);
	}
	
	private int getCentro(int width) {
		return (ancho / 2) -( width / 2);
	}
	
	private void limpiarFrame() {
		remove(facilBtn);
		remove(normalBtn);
		remove(dificilBtn);
	}
	
	private void iniciarPanel(int tamanio) {
		panelGrafico = new PanelGrafico(tamanio );	 
		add(panelGrafico);
		revalidate();
        repaint();
	}

	private static Font loadCustomFont() {
        try {
			File fontFile = new File(View.class.getResource("./local/fuente/pixel_font.ttf").getPath());

            Font customFont = Font.createFont(Font.TRUETYPE_FONT, fontFile);
            return customFont.deriveFont(Font.PLAIN, 14); // Personaliza el tamaño y estilo de la fuente
        } catch (IOException | FontFormatException e) {
            e.printStackTrace();
            return new Font("SansSerif", Font.PLAIN, 14);
        }
    }

    private static void setUIFont(Font customFont) {
        UIManager.put("Button.font", customFont);
        UIManager.put("Label.font", customFont);
        UIManager.put("TextField.font", customFont);
    }
}