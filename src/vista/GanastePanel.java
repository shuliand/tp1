package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controlador.MejoresPuntosController;

@SuppressWarnings("serial")
public class GanastePanel extends JPanel{
    private MejoresPuntosController mejoresPuntosController;
    private boolean usuarioEntraEnTop;
    public GanastePanel(int cantClick) {
        setBounds(0, 0, 1200, 800);
        setLayout(null);
        setOpaque(false);

        mejoresPuntosController = new MejoresPuntosController(cantClick);
        usuarioEntraEnTop = mejoresPuntosController.getEntraEnTop();
        establecerTopLabel();
        establecerRecord();
        if(usuarioEntraEnTop) {
            crearTecladoListener();
        }
    }
    private void establecerRecord() {
        for (int i = 0; i < 10; i++) {

            String nombre = mejoresPuntosController.getNombresTop().get(i);
            String puntos = mejoresPuntosController.getPuntosTop().get(i) + "";

            int cantidadDeEspacios = 4 - puntos.length();
            //rellando para que todos tengan 4 de longitud!
            if (cantidadDeEspacios > 0) {
                StringBuilder espacios = new StringBuilder();
                for (int j = 0; j < cantidadDeEspacios; j++) {
                    espacios.append(" ");
                }
                puntos =  puntos + espacios ;
            }

            int cantidadCeros = 4 - puntos.length();
            //rellando para que todos tengan 4 de longitud!
            if (cantidadCeros > 0) {
                StringBuilder ceros = new StringBuilder();
                for (int j = 0; j < cantidadCeros; j++) {
                    ceros.append("0");
                }
                puntos = ceros.toString() + puntos;
            }
            String label = (i+1)+"."+ nombre +" : " +puntos ;
            JLabel jLabel = new JLabel();
            crearLabelCentrado(jLabel, label, 20, 250 + i * 40);
            add(jLabel);
        }
    }

    private void establecerTopLabel(){
        String textoTop = usuarioEntraEnTop? "Felicidades has entrado en el top!": "Lamentablemente no has logrado entrar al top";
        JLabel jLabelTop = new JLabel();
        crearLabelCentrado(jLabelTop, textoTop, 30, 60);
        
        if(usuarioEntraEnTop) {
            String textoNombreUsuario= "Escribe tu nombre para guardarlo:";
            JLabel jLabelNombreUsuario =new JLabel();
            crearLabelCentrado(jLabelNombreUsuario, textoNombreUsuario, 20, 130);
            
            add(jLabelNombreUsuario);
            add(jLabelNombreUsuario);

        }
        add(jLabelTop);
    }
    private void crearTecladoListener() {
        JTextField j = new JTextField();
        j.setBounds(500, 200, 200, 50);
        j.addKeyListener(new KeyAdapter  
        () {
            @Override
            public void keyPressed(KeyEvent e) {
                    int keyCode = e.getKeyCode();
                    if (keyCode == KeyEvent.VK_BACK_SPACE || keyCode == KeyEvent.VK_DELETE) {
                        mejoresPuntosController.eliminarLetra();
                    } else if(keyCode == KeyEvent.VK_ENTER) {
                        mejoresPuntosController.guardarNuevoTop();
                    }
                    else {
                        if (Character.isLetter(e.getKeyChar())) {
                           mejoresPuntosController.agregarLetra(KeyEvent.getKeyText(keyCode));;
                        } 
                    }
                    j.setText(mejoresPuntosController.getNombreDeUsuario());
                    j.repaint();
            }
        });
        add(j);
    }
    
    private void crearLabelCentrado(JLabel label,String texto,int size,int y) {
      label.setText(texto);
      label.setForeground(Color.white);
      label.setHorizontalAlignment(SwingConstants.CENTER);
      label.setBounds(0, y, 1200, 100);
      label.setFont(Fuente.cargarFuente().deriveFont(Font.BOLD, size));
    }
}
