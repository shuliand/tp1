package vista;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class CeldaDeLuzBtn extends JButton {
    private boolean prendida;

    public CeldaDeLuzBtn(boolean prendida) {
        setBorder(new LineBorder(Color.BLACK, 5));
        this.prendida = prendida;
        setColor(this.prendida);
    } 
    
    private void setColor(boolean prendida) {
        if (prendida) {
            setBackground(Color.white);
        }else {
            setBackground(Color.gray);
        }	
    }

    public void cambiarColor (boolean prendida) {
        setColor(prendida);
    }
}
