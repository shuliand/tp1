package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import controlador.Controller;
import modelo.Luz;


@SuppressWarnings({ "serial" })
public class PanelGrafico extends JPanel {
	private Controller controlador;
	private JPanel tablero;
	private CeldaDeLuzBtn [][] tableroBotones;
	int cantClick;
	JLabel movimientosLabel;

	public PanelGrafico(int tamanio ) {
		this.controlador = new Controller(tamanio);
		this.tableroBotones= new CeldaDeLuzBtn [tamanio][tamanio];
		this.cantClick = 0;
		setBounds(0, 0, 1200, 800);
		setLayout(null);
		setOpaque(false);
		
		movimientosLabel= new JLabel();
		movimientosLabel.setForeground(new Color(255, 0, 0)); 
		movimientosLabel.setFont(Fuente.cargarFuente().deriveFont(Font.BOLD, 20));
		movimientosLabel.setBounds(100, 33, 764, 39);
		movimientosLabel.setText("MOVIMIENTOS: " + cantClick);
			
		tablero = new JPanel();
		tablero.setOpaque(false);
		tablero.setBounds(300, 100, 600, 600);
		tablero.setLayout(new GridLayout(tamanio,tamanio,5,5));
		crearTablero();

		add(movimientosLabel);
		add(tablero);

	}
		
	private void mensajeGanador() {
		removeAll();
		repaint();
		GanastePanel panel = new GanastePanel(this.cantClick);
		add(panel);	
		repaint();
	}

	private void redibujarTablero() {
		for (int i = 0; i < tableroBotones.length; i++) {
			for (int j = 0; j < tableroBotones[i].length; j++) {
				boolean estaPrendida = controlador.luzEstaPrendida(i, j);
				CeldaDeLuzBtn celda =  tableroBotones[i][j];
				celda.cambiarColor(estaPrendida);
			}
		}
	}

	private void crearTablero() {
		Luz[][] matriz = this.controlador.getTablero();
		for (int fila = 0; fila < matriz.length; fila++) {
			for (int col = 0; col < matriz[fila].length; col++) {
				boolean estaPrendida = controlador.luzEstaPrendida(fila, col);
				CeldaDeLuzBtn boton = new CeldaDeLuzBtn(estaPrendida);
				int filaActual = fila;
				int colActual = col;
				tableroBotones[fila][col] = boton;
				boton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						actualizarTablero(filaActual, colActual);
					}
				});
				tablero.add(boton);
			}
		}
	}
	
	private void actualizarTablero(int filaActual, int colActual) {
		 cantClick ++;
		    cambiarClicks(cantClick);
		    controlador.alternar(filaActual,colActual);
		    redibujarTablero();
		    if (controlador.getGanaste()) {
		    	mensajeGanador();
		    }
		    tablero.repaint();
	}
	
	private void cambiarClicks(int cantClick) {
		movimientosLabel.setText("MOVIMIENTOS: " + cantClick);
        repaint();
    }

}
